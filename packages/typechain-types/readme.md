# typechain-types@0.0.1

osp-client-js is a osp clent js sdk.

## Installation

```bash

pnpm add typechain-types
```

or

```bash
npm i typechain-types --save-dev
```

## Usage

```python

```

# Changelog

## [0.0.1]

### Change

- init commit

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
