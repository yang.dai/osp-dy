import {
  SendUserOperationResult,
  UserOperationReceipt,
} from "@alchemy/aa-core";
import { log } from "@web3auth/base";
import { BytesLike } from "ethers";
import { hexlify } from "ethers/lib/utils";
import { Hex, InvalidParamsRpcError, RpcRequestError } from "viem";
import {
  ACCOUNT_TYPE,
  CHAIN_MAP,
  CONTRACT_REVERT_MAP,
  OspRes,
  WALLET_ERROR,
} from "../constant";
import { Multicall, Multicall__factory } from "typechain-types";
import { Wallet } from "../wallet";
import { SimpleSmartAccountOwner, SmartAccount } from "./account";
import { OspVerifyPaymaster } from "./paymaster";
import { AAProvider } from "./provider";

export type UserOp = {
  to?: string;
  data: BytesLike;
  value?: bigint;
};
export type BatchUserOp = UserOp[];

const resHandler = (result: SendUserOperationResult): OspRes => {
  console.log("sendUserOperation result: ", JSON.stringify(result));
  return { data: { ...result } };
};

const errorhandler = (e: Error): OspRes => {
  console.log(e);
  if (e instanceof RpcRequestError) {
    console.log("RpcRequestError: ", e.details);
    if (e.details?.includes("user operation's call reverted:")) {
      const signature = e.details
        .split("user operation's call reverted: ")[1]
        .trim();
      console.log("signature: ", signature);
      let mapError = CONTRACT_REVERT_MAP[signature];
      return { error: { ...e, code: mapError } };
    }
  } else if (e instanceof InvalidParamsRpcError) {
    if (e.code == -32602) {
      return { error: { ...e, code: WALLET_ERROR.GAS_NOT_SUFFICIENT } };
    }
  }
  return { error: { code: WALLET_ERROR.UNKNOWN_ERROR, ...e } };
};

const covertUserOp = function (userOp: UserOp, ospAddress: string) {
  return {
    target: <`0x${string}`>(userOp.to ?? ospAddress),
    data: <`0x${string}`>hexlify(userOp.data),
    value: userOp.value ?? 0n,
  };
};

export class OspAccount {
  provider: AAProvider<any>;
  address: string;
  wallet: Wallet;
  accountType: ACCOUNT_TYPE;
  mulcall: Multicall;

  constructor(wallet: Wallet, accountType: ACCOUNT_TYPE = ACCOUNT_TYPE.AA) {
    this.wallet = wallet;
    this.accountType = accountType;
  }

  async init() {
    if (this.accountType === ACCOUNT_TYPE.AA) {
      const owner: SimpleSmartAccountOwner = {
        // this should sign a message according to ERC-191
        signMessage: async (msg) =>
          <"0x${string}">(await this.wallet.personalSign(msg)).data,
        getAddress: async () => <"0x${string}">this.wallet.eoaAddress,
      };
      this.provider = new AAProvider(
        this.wallet.contractConfig.alchemy_api, // rpcUrl
        <`0x${string}`>this.wallet.contractConfig.entry_point_address, // entryPointAddress
        CHAIN_MAP[this.wallet.contractConfig.chain_id], // chain
        new OspVerifyPaymaster(this.wallet) // paymaster
      ).connect(
        (rpcClient) =>
          new SmartAccount({
            entryPointAddress: <`0x${string}`>(
              this.wallet.contractConfig.entry_point_address
            ),
            chain: CHAIN_MAP[this.wallet.contractConfig.chain_id],
            factoryAddress: <`0x${string}`>(
              this.wallet.contractConfig.account_factory_address
            ),
            rpcClient,
            owner,
            // optionally if you already know the account's address
            // accountAddress: "0xFAF9c42cd05063fe22A966a0886BD48e51Eef9F1",
          })
      );
      this.address = await this.provider.account.getAddress();
    } else {
      this.mulcall = Multicall__factory.connect(
        this.wallet.contractConfig.osp,
        this.wallet.etherProvider.getSigner()
      );
      this.address = this.wallet.eoaAddress;
    }
  }

  async sendOpAndGetResult(
    userOp: BatchUserOp | UserOp,
    onCompleted = () => {}
  ): Promise<OspRes> {
    //这里现在默认只是AA
    if (this.accountType === ACCOUNT_TYPE.AA) {
      //send operation
      const callData = Array.isArray(userOp)
        ? userOp.map((item) =>
            covertUserOp(item, this.wallet.contractConfig.osp)
          )
        : covertUserOp(userOp, this.wallet.contractConfig.osp);
      console.log(callData);
      const res = await this.provider
        .sendUserOperation(callData)
        .then(resHandler)
        .catch(errorhandler);
      // sync operation
      if (!res?.error) {
        this.whenOpCompleted(res.data?.hash, onCompleted);
      }
      return res;
    } else {
      // 这里先默认都是OSP
      let datas = Array.isArray(userOp)
        ? userOp.map((item) => {
            return item.data;
          })
        : [userOp.data];
      let res = await this.mulcall
        .multicall(datas)
        .then((res) => {
          return { data: { ...res } };
        })
        .catch(errorhandler);
      this.whenTxCompleted(res.data?.transactionHash, onCompleted);
      return res;
    }
  }

  async getAAAddress(): Promise<string> {
    return this.address || this.provider.account.getAddress();
  }

  async whenTxCompleted(txHash: Hex, onCompleted = () => {}) {
    this.wallet.etherProvider
      .getTransactionReceipt(txHash)
      .then(async (receipt) => {
        console.log("getTransactionReceipt result: ", JSON.stringify(receipt));
        if (receipt?.status == 1) {
          await this.wallet.request.call(["tx", "txStore"], {
            tx_hash: receipt?.transactionHash,
          });
          onCompleted();
        }
      });
  }

  async whenOpCompleted(opHash: Hex, onCompleted = () => {}) {
    let result: UserOperationReceipt = null;
    while (result === null) {
      result = await this.provider
        .getUserOperationReceipt(opHash)
        .catch((e) => {
          console.log("getUserOperationReceipt error", e);
          return null;
        })
        .then((res) => {
          return res;
        });
      if (result) {
        break;
      }
      log.info("waiting for tx to be handled...");
      await new Promise((resolve) => setTimeout(resolve, 1000)); // 每隔1秒轮询一次
    }
    console.log("getUserOperationReceipt result: ", JSON.stringify(result));
    if (result.success && result.receipt?.status == 1) {
      // 上链成功后调用下store 并且触发回调函数
      await this.wallet.request.call(["tx", "txStore"], {
        tx_hash: result.receipt?.transactionHash,
      });
      onCompleted();
    }
  }
}
