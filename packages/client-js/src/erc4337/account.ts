import {
  Address,
  BaseSmartAccountParams,
  BaseSmartContractAccount,
  BatchUserOperationCallData,
  Hex,
} from "@alchemy/aa-core";
import {
  FallbackTransport,
  Transport,
  concatHex,
  encodeFunctionData,
  hexToBytes,
} from "viem";
import { account_abi, factory_abi } from "../abi/account";

export interface SimpleSmartAccountOwner {
  signMessage: (msg: Uint8Array) => Promise<string>;
  getAddress: () => Promise<string>;
}

export interface SimpleSmartAccountParams<
  TTransport extends Transport | FallbackTransport = Transport
> extends BaseSmartAccountParams<TTransport> {
  owner: SimpleSmartAccountOwner;
  factoryAddress: Address;
  index?: bigint;
}

export class SmartAccount<
  TTransport extends Transport | FallbackTransport = Transport
> extends BaseSmartContractAccount<TTransport> {
  private owner: SimpleSmartAccountOwner;
  private factoryAddress: Address;
  constructor(params: SimpleSmartAccountParams) {
    super(params);
    this.owner = params.owner;
    this.factoryAddress = params.factoryAddress;
  }
  getDummySignature(): `0x${string}` {
    return "0xfffffffffffffffffffffffffffffff0000000000000000000000000000000007aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa1c";
  }
  async encodeExecute(
    target: string,
    value: bigint,
    data: string
  ): Promise<`0x${string}`> {
    return encodeFunctionData({
      abi: account_abi,
      functionName: "executeBatch",
      args: [[target], [value], [data]],
    });
  }
  override async encodeBatchExecute(
    _txs: BatchUserOperationCallData
  ): Promise<`0x${string}`> {
    const [targets, values, datas] = _txs.reduce(
      (accum, curr) => {
        accum[0].push(curr.target);
        accum[1].push(curr.value || 0n);
        accum[2].push(curr.data);
        return accum;
      },
      [[], [], []] as [Address[], bigint[], Hex[]]
    );

    return encodeFunctionData({
      abi: account_abi,
      functionName: "executeBatch",
      args: [targets, values, datas],
    });
  }
  async signMessage(msg: Uint8Array | string): Promise<`0x${string}`> {
    if (typeof msg === "string" && msg.startsWith("0x")) {
      msg = hexToBytes(msg as Hex);
    } else if (typeof msg === "string") {
      msg = new TextEncoder().encode(msg);
    }
    let res = <`0x${string}`>await this.owner.signMessage(msg);
    return res;
  }

  protected async getAccountInitCode(): Promise<`0x${string}`> {
    return concatHex([
      this.factoryAddress,
      encodeFunctionData({
        abi: factory_abi,
        functionName: "createAccount",
        args: [await this.owner.getAddress()],
      }),
    ]);
  }
}
