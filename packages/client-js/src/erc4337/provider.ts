import {
  AccountMiddlewareFn,
  Address,
  BaseSmartContractAccount,
  deepHexlify,
  PublicErc4337Client,
  resolveProperties,
  SmartAccountProvider,
  SmartAccountProviderOpts,
  SupportedTransports,
  UserOperationStruct,
} from "@alchemy/aa-core";

import { Chain } from "viem";
import { IPaymaster } from "./paymaster";

export class AAProvider<
  TTransport extends SupportedTransports
> extends SmartAccountProvider<TTransport> {
  paymaster?: IPaymaster;
  constructor(
    rpcProvider: string | PublicErc4337Client<TTransport>,
    protected entryPointAddress: Address,
    protected chain: Chain,
    paymaster?: IPaymaster,
    readonly account?: BaseSmartContractAccount<TTransport>,
    opts?: SmartAccountProviderOpts
  ) {
    super(rpcProvider, entryPointAddress, chain, account, opts);
    this.paymaster = paymaster;
  }
  // These are dependent on the specific paymaster being used
  // You should implement your own middleware to override these
  // or extend this class and provider your own implemenation
  readonly dummyPaymasterDataMiddleware: AccountMiddlewareFn = async (
    struct: UserOperationStruct
  ): Promise<UserOperationStruct> => {
    if (this.paymaster)
      return this.paymaster.dummyPaymasterDataMiddleware(struct);
    struct.paymasterAndData = "0x";
    return struct;
  };

  readonly paymasterDataMiddleware: AccountMiddlewareFn = async (
    struct: UserOperationStruct
  ): Promise<UserOperationStruct> => {
    if (this.paymaster) return this.paymaster.paymasterDataMiddleware(struct);
    struct.paymasterAndData = "0x";
    return struct;
  };

  readonly gasEstimator: AccountMiddlewareFn = async (struct) => {
    const request = deepHexlify(await resolveProperties(struct));
    const estimates = await this.rpcClient.estimateUserOperationGas(
      request,
      this.entryPointAddress
    );

    struct.callGasLimit = estimates.callGasLimit;
    struct.preVerificationGas = estimates.preVerificationGas;
    struct.verificationGasLimit =
      BigInt(estimates.verificationGasLimit) + 21000n;
    // struct.initCode == "0x"
    //  ? estimates.verificationGasLimit

    return struct;
  };
}
